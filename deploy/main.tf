terraform {
  backend "s3" {
    bucket         = "hello-recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "ap-northeast-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "ap-northeast-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tag = {
    Enviroment = terraform.workspace
    Project    = var.project
    Owner      = var.contact
    ManagedBy  = "Terraform"
  }
}
